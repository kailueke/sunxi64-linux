From: Samuel Holland <samuel@sholland.org>
Date: Sat, 13 Mar 2021 23:53:00 -0600
Subject: arm64: dts: allwinner: Add sun4i MMIO timer nodes

For a CPU to enter an idle state, some timer must be available to
trigger an IRQ and wake it back up. The local ARM architectural timer is
not sufficient, because that timer stops when the CPU is powered down.
The ARM architectural timer from some other CPU can be used, but doing
so prevents that other CPU from entering an idle state. For all CPUs to
power down at the same time, Linux needs a timer which is not tied to
any CPU.

Hook up the "sun4i" timer so it can be used for this purpose. It runs at
24 MHz, which balances resolution and power consumption.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 arch/arm64/boot/dts/allwinner/sun50i-a64.dtsi | 9 +++++++++
 arch/arm64/boot/dts/allwinner/sun50i-h6.dtsi  | 9 +++++++++
 2 files changed, 18 insertions(+)

diff --git a/arch/arm64/boot/dts/allwinner/sun50i-a64.dtsi b/arch/arm64/boot/dts/allwinner/sun50i-a64.dtsi
index e25f14f..b41f944 100644
--- a/arch/arm64/boot/dts/allwinner/sun50i-a64.dtsi
+++ b/arch/arm64/boot/dts/allwinner/sun50i-a64.dtsi
@@ -819,6 +819,15 @@ uart4_rts_cts_pins: uart4-rts-cts-pins {
 			};
 		};
 
+		timer@1c20c00 {
+			compatible = "allwinner,sun50i-a64-timer",
+				     "allwinner,sun8i-a23-timer";
+			reg = <0x01c20c00 0xa0>;
+			interrupts = <GIC_SPI 18 IRQ_TYPE_LEVEL_HIGH>,
+				     <GIC_SPI 19 IRQ_TYPE_LEVEL_HIGH>;
+			clocks = <&osc24M>;
+		};
+
 		wdt0: watchdog@1c20ca0 {
 			compatible = "allwinner,sun50i-a64-wdt",
 				     "allwinner,sun6i-a31-wdt";
diff --git a/arch/arm64/boot/dts/allwinner/sun50i-h6.dtsi b/arch/arm64/boot/dts/allwinner/sun50i-h6.dtsi
index 4592fb7..91f6fa4 100644
--- a/arch/arm64/boot/dts/allwinner/sun50i-h6.dtsi
+++ b/arch/arm64/boot/dts/allwinner/sun50i-h6.dtsi
@@ -271,6 +271,15 @@ cpu_speed_grade: cpu-speed-grade@1c {
 			};
 		};
 
+		timer@3009000 {
+			compatible = "allwinner,sun50i-h6-timer",
+				     "allwinner,sun8i-a23-timer";
+			reg = <0x03009000 0xa0>;
+			interrupts = <GIC_SPI 48 IRQ_TYPE_LEVEL_HIGH>,
+				     <GIC_SPI 49 IRQ_TYPE_LEVEL_HIGH>;
+			clocks = <&osc24M>;
+		};
+
 		watchdog: watchdog@30090a0 {
 			compatible = "allwinner,sun50i-h6-wdt",
 				     "allwinner,sun6i-a31-wdt";
